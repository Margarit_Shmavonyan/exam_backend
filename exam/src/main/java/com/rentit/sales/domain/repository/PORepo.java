package com.rentit.sales.domain.repository;

import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.sales.domain.model.PurchaseOrder;

import java.time.LocalDate;

public interface PORepo {
    PurchaseOrder findPOwithDefectedPlant(PlantInventoryItem item, LocalDate repairPeriodStart, LocalDate repairPeriodEnd);

}
