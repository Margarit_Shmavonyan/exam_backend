package com.rentit.sales.domain.model;

public enum POStatus {
    PENDING,
    REJECTED,
    OPEN,
    CLOSED,
    PENDING_EXTENSION,
    PLANT_RETURNED,
    PLANT_ON_REPAIR}
