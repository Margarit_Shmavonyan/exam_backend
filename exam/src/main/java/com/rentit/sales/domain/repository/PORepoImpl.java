package com.rentit.sales.domain.repository;

import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

public class PORepoImpl implements PORepo {

    @Autowired
    EntityManager em;

    //    @Override
    public List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select i.plantInfo from PlantInventoryItem i where lower(i.plantInfo.name) like concat('%', ?1, '%') and i not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)"
                , PlantInventoryEntry.class)
                .setParameter(1, name.toLowerCase())
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

    @Override
    public PurchaseOrder findPOwithDefectedPlant(PlantInventoryItem item, LocalDate repairPeriodStart, LocalDate repairPeriodEnd) {
        return em.createQuery(" select po from PurchaseOrder po where po.item = ?1 and po.rentalPeriod in " +
                        "(select r.schedule from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)"
                , PurchaseOrder.class)
                .setParameter(1, item)
                .setParameter(2, repairPeriodStart)
                .setParameter(3, repairPeriodEnd)
                .getSingleResult();
    }
}

