package com.rentit.sales.rest.assemblers;

import com.rentit.common.rest.ExtendedLink;
import com.rentit.common.rest.dto.BusinessPeriodDTO;
import com.rentit.inventory.rest.assemblers.PlantInventoryEntryAssembler;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.rest.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.rest.controllers.SalesRestController;
import org.eclipse.jetty.http.HttpMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.afford;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PurchaseOrderAssembler {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public Resource<PurchaseOrderDTO> toResource(PurchaseOrder po) {
        PurchaseOrderDTO dto = new PurchaseOrderDTO();
        dto.set_id(po.getId());
        if (po.getPlant() != null)
            dto.setPlant(plantInventoryEntryAssembler.toResource(po.getPlant()));
        dto.setRentalPeriod(BusinessPeriodDTO.of(po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate()));
        dto.setTotal(po.getTotal());
        dto.setStatus(po.getStatus());

        return new Resource<>(
                dto,
                checkingEqupCond (po)
                );
    }

    public Resources<Resource<PurchaseOrderDTO>> toResources(List<PurchaseOrder> orders) {
        return new Resources<>(
                orders.stream().map(o -> toResource(o)).collect(Collectors.toList()),
                linkTo(methodOn(SalesRestController.class).getAllPurchaseOrders()).withSelfRel()
                .andAffordance(afford(methodOn(SalesRestController.class).createPurchaseOrder(null)))
        );
    }

    private List<Link> checkingEqupCond(PurchaseOrder order) {
        switch (order.getStatus()) {
            case PLANT_ON_REPAIR:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel(),
                        linkTo(methodOn(SalesRestController.class).PORepairs(order.getId()))
                                .withRel("PLANT_ON_REPAIR")
                );
            case PLANT_RETURNED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel(),
                        linkTo(methodOn(SalesRestController.class).POWithoutRepairs(order.getId()))
                                .withRel("PLANT_RETURNED")
                );
            case PENDING:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel(),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).allocatePlant(order.getId())).toString(), "accept", HttpMethod.POST),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).rejectPurchaseOrder(order.getId())).toString(), "reject", HttpMethod.DELETE)
                );
            case OPEN:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel(),
                        new ExtendedLink(linkTo(methodOn(SalesRestController.class).handleDeleteOnPurchaseOrder(order.getId())).toString(), "close", HttpMethod.DELETE),
                        linkTo(methodOn(SalesRestController.class).retrievePurchaseOrderExtensions(order.getId()))
                                .withRel("extensions")
                        .andAffordance(afford(methodOn(SalesRestController.class).requestPurchaseOrderExtension(null, order.getId())))
                );
            case PENDING_EXTENSION:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel(),
                        linkTo(methodOn(SalesRestController.class).retrievePurchaseOrderExtensions(order.getId()))
                                .withRel("extensions")
                );
            case REJECTED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel()
                                .andAffordance(afford(methodOn(SalesRestController.class).resubmitPurchaseOrder(order.getId(), null)))
                );
            case CLOSED:
                return Arrays.asList(
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(order.getId()))
                                .withSelfRel()
                );
        }
        return Collections.emptyList();
    }
}