package com.rentit.inventory.rest.assemblers;

import com.rentit.inventory.rest.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.rest.controllers.InventoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PlantInventoryEntryAssembler {

    public Resource<PlantInventoryEntryDTO> toResource(PlantInventoryEntry plant) {
        PlantInventoryEntryDTO dto = new PlantInventoryEntryDTO();
        dto.set_id(plant.getId());
        dto.setName(plant.getName());
        dto.setDescription(plant.getDescription());
        dto.setPrice(plant.getPrice());

        return new Resource<>(
                dto,
                linkTo(methodOn(InventoryRestController.class).getPlantInventoryEntry(plant.getId()))
                        .withSelfRel()
        );
    }

    public Resources<Resource<PlantInventoryEntryDTO>> toResources(List<PlantInventoryEntry> plants) {
        return new Resources<>(
                plants.stream().map(plant -> toResource(plant)).collect(Collectors.toList())
        );
    }
}