package com.rentit.inventory.rest.controllers;

import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
public class PlantInventoryItemRestController {

    @GetMapping("/items/{id}")
    public Resource<?> findPlant(@PathVariable("id") Long id) {
        return null;
    }

    @PostMapping("/item/{id}/servicable")
    public Resource<?> servicable(@PathVariable("id") Long id){
        return null;
    }

    @PostMapping("/item/{id}/unservicable")
    public Resource<?> unservicable(@PathVariable("id") Long id){
        return null;
    }

}
