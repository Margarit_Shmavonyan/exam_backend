package com.rentit.inventory.rest.assemblers;

import com.rentit.inventory.rest.dto.PlantInventoryItemDTO;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.rest.controllers.PlantInventoryItemRestController;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PlantInventoryItemAssembler {

    public Resource<PlantInventoryItemDTO> toResource(PlantInventoryItem plant) {
        PlantInventoryItemDTO dto = new PlantInventoryItemDTO();
        dto.set_id(plant.getId());
        return new Resource<>(
                dto,
                checkingEqupCond(plant)
        );
    }

    private List<Link> checkingEqupCond(PlantInventoryItem item) {
        switch (item.getEquipmentCondition()) {
            case SERVICEABLE:
                return Arrays.asList(
                        linkTo(methodOn(PlantInventoryItemRestController.class).findPlant(item.getId())).withSelfRel()
                        ,
                        linkTo(methodOn(PlantInventoryItemRestController.class).servicable(item.getId()))
                                .withRel("PLANT_RETURNED")
                );
            case UNSERVICABLE_REPAIRABLE:
                return Arrays.asList(
                        linkTo(methodOn(PlantInventoryItemRestController.class).findPlant(item.getId())).withSelfRel()
                        ,
                        linkTo(methodOn(PlantInventoryItemRestController.class).unservicable(item.getId()))
                                .withRel("PLANT_UNSERVICABLE")
                );
        }
        return Collections.emptyList();
    }

    public Resources<Resource<PlantInventoryItemDTO>> toResources(List<PlantInventoryItem> items) {
        return new Resources<>(
                items.stream().map(i -> toResource(i)).collect(Collectors.toList())
        );
    }



}