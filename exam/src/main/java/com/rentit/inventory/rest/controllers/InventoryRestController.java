package com.rentit.inventory.rest.controllers;

import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
public class InventoryRestController {

    @GetMapping("/plants/{id}")
    public Resource<?> getPlantInventoryEntry(@PathVariable("id") Long id) {
        return null;
    }
}
