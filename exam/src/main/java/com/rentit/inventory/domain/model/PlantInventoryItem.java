package com.rentit.inventory.domain.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class PlantInventoryItem {
    @Id @GeneratedValue
    Long id;

    String serialNumber;
    @Enumerated(EnumType.STRING)
    EquipmentCondition equipmentCondition;

    @ManyToOne
    PlantInventoryEntry plantInfo;

    public void serviceable() {
        this.equipmentCondition = EquipmentCondition.SERVICEABLE;
    }

    public void unserviceableRepairable() {
        this.equipmentCondition = EquipmentCondition.UNSERVICABLE_REPAIRABLE;
    }

}
