package com.rentit.inventory.domain.model;

public enum  EquipmentCondition { SERVICEABLE, UNSERVICABLE_REPAIRABLE }
